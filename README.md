Code for controlling the motors on the Arduino.

For Arduino:
Define BUILD_FOR_DRIVETRAIN_ARDUINO in `main.h` for drivetrain.
Define BUILD_FOR_ARM_ARDUINO in `main.h` for arm.
Build with the Arduino IDE.

http://wiki.ros.org/joy/Tutorials/ConfiguringALinuxJoystick  
connect to HC-06, when prompted type 1234  
sudo rfcomm bind 0 00:14:03:05:F1:F5  
Now the bluetooth module is avaiable as a serial port  

# Todo
- Add vision to searching module
- Add aggressive search
- Test
