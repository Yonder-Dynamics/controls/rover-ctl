#pragma once

static float clamp(float v, float lo, float hi)
{
  return max(lo, min(hi, v));
}

static float lerp(float x, float y, float alpha)
{
  return (1.0f - alpha) * x + alpha * y;
}

static float remap(float x, float input_min, float input_max, float output_min, float output_max)
{
  x = clamp(x, input_min, input_max);
  float alpha = float(x - input_min) / float(input_max - input_min);
  return lerp(output_min, output_max, alpha);
}

float linearRemap(float value, float calA, float calB)
{
  return calA * value + calB;
}