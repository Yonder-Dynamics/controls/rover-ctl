#define NUM_MOTORS 6
#define NUM_MOTOR_CMDS 2 // the left and right three wheels are grouped together for now

#define INTPUT_MIN -255
#define INTPUT_MAX 255
#define OUTPUT_MIN 35
#define OUTPUT_MAX 150
#define ZERO_OUTPUT OUTPUT_MIN / 2 + OUTPUT_MAX / 2

#define FRONT_RIGHT_PIN 11
#define MIDDLE_RIGHT_PIN 10
#define BACK_RIGHT_PIN 9
#define FRONT_LEFT_PIN 6
#define MIDDLE_LEFT_PIN 5
#define BACK_LEFT_PIN 3

#define LOOP_RATE 100 // [Hz]
#define TIMEOUT 1000 // [ms]

const unsigned char motor_pins[] = {
  FRONT_LEFT_PIN,
  MIDDLE_LEFT_PIN,
  BACK_LEFT_PIN,
  FRONT_RIGHT_PIN,
  MIDDLE_RIGHT_PIN,
  BACK_RIGHT_PIN
};

const int motor_polarity[] = {1, 1, 1, -1, -1, -1};

enum modes {
  DISABLED,
  MANUAL,
  AUTONOMOUS
};
