// #define USE_USBCON  // uncomment for stm32

#include <Servo.h>
#include <ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/String.h>
#include "consts.h"
#include "math.h"

/***** ros stuff *****/
ros::NodeHandle nh;                                  // node handle
unsigned int motor_cmds[NUM_MOTORS] = {ZERO_OUTPUT}; // initialize to zero throttle
unsigned long last_time_received = 0;

modes state = modes::DISABLED ;  // start out disabled

void on_state(const std_msgs::String &msg) {
  if (strcmp(msg.data, "manual") == 0) state = modes::MANUAL;
  else if (strcmp(msg.data, "autonomous") == 0) state = modes::AUTONOMOUS;
  else if (strcmp(msg.data, "off") == 0) state = modes::DISABLED;
}

void on_input(modes required_mode, const std_msgs::Float64MultiArray &msg) {
  if (state != required_mode) return;  // if mode doesn't match, abort
  static const int motors_per_cmd = NUM_MOTORS / NUM_MOTOR_CMDS;
  for (int i = 0; i < NUM_MOTOR_CMDS; i++)
    for (int j = i * motors_per_cmd; j < (i + 1) * motors_per_cmd; j++)
      motor_cmds[j] = remap(motor_polarity[j] * msg.data[i], INTPUT_MIN, INTPUT_MAX, OUTPUT_MIN, OUTPUT_MAX);
  last_time_received = millis();
}

ros::Subscriber<std_msgs::Float64MultiArray> manual_sub(
  "/manual_drive_train",
  [](const std_msgs::Float64MultiArray &msg) { on_input(modes::MANUAL, msg); }
);
ros::Subscriber<std_msgs::Float64MultiArray> auto_sub(
  "/autonomous_drive_train",
  [](const std_msgs::Float64MultiArray &msg) { on_input(modes::AUTONOMOUS, msg); }
);
// std_msgs::Float64MultiArray feedback_array;
// ros::Publisher feedback_pub("/drive_feedback", &feedback_array);
ros::Subscriber<std_msgs::String> state_sub("/state", &on_state);

/***** drive motors *****/
Servo motors[NUM_MOTORS];

void setup()
{
  pinMode(LED_BUILTIN, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  for (int i = 0; i < NUM_MOTORS; i++){
    motors[i].attach(motor_pins[i]);
    motors[i].write(ZERO_OUTPUT);
  // feedback_array.data_length = NUM_MOTORS;
  // feedback_array.data = new float[NUM_MOTORS];
  }
  nh.initNode();
  nh.subscribe(manual_sub);
  nh.subscribe(auto_sub);
  nh.subscribe(state_sub);
  // nh.advertise(feedback_pub);
}

void motor_safety() {
  if (millis() - last_time_received > TIMEOUT) // timout safety
    for (int i = 0; i < NUM_MOTORS; i++)
      motor_cmds[i] = ZERO_OUTPUT;
}

void loop()
{
  motor_safety();

  for (int i = 0; i < NUM_MOTORS; i++) {
    motors[i].write(motor_cmds[i]); // drive each motor
    // feedback_array.data[i] = motor_cmds[i];
  }
  // feedback_pub.publish(&feedback_array);
  
  nh.spinOnce();  // must be called for rosserial to work
  delay(1000/LOOP_RATE);
}
