#include <Servo.h>
#include "math.h"
#include "System.h"
#include "Motor.h"
#include "Message.h"

#define BUILD_FOR_DRIVETRAIN_ARDUINO
//#define BUILD_FOR_ARM_ARDUINO

static char messageBuffer[sizeof(Message)];
static unsigned long lastReceived = millis();

constexpr unsigned char DRIVETRAIN_FRONT_RIGHT_PIN = 2;
constexpr unsigned char DRIVETRAIN_MIDDLE_RIGHT_PIN = 3;
constexpr unsigned char DRIVETRAIN_BACK_RIGHT_PIN = 5;
constexpr unsigned char DRIVETRAIN_FRONT_LEFT_PIN = 10;
constexpr unsigned char DRIVETRAIN_MIDDLE_LEFT_PIN = 11;
constexpr unsigned char DRIVETRAIN_BACK_LEFT_PIN = 12;

constexpr unsigned char LED_R = A2;
constexpr unsigned char LED_G = A3;
constexpr unsigned char LED_B = 9;

constexpr unsigned char ARM_SHOULDER_PIN = 3; //vex
constexpr unsigned char ARM_ELBOW_PIN = 5; //vex
constexpr unsigned char ARM_BASE_PIN = 9; //vex
constexpr unsigned char ARM_WRIST_L = 11; 
constexpr unsigned char ARM_WRIST_L_DIR1 = 12; 
constexpr unsigned char ARM_WRIST_L_DIR2 = 7; 
constexpr unsigned char ARM_WRIST_R = 6; 
constexpr unsigned char ARM_WRIST_R_DIR1 = 2; 
constexpr unsigned char ARM_WRIST_R_DIR2 = 8; 
constexpr unsigned char GRIPPER = 10; //servo

constexpr uint32_t OFF = 0;
constexpr uint32_t MANUAL = 1;
constexpr uint32_t AUTONOMOUS = 2;

#if defined BUILD_FOR_DRIVETRAIN_ARDUINO
Motor motors[] = {
  Motor(DRIVETRAIN_FRONT_RIGHT_PIN),
  Motor(DRIVETRAIN_MIDDLE_RIGHT_PIN),
  Motor(DRIVETRAIN_BACK_RIGHT_PIN),
  Motor(DRIVETRAIN_FRONT_LEFT_PIN),
  Motor(DRIVETRAIN_MIDDLE_LEFT_PIN),
  Motor(DRIVETRAIN_BACK_LEFT_PIN)
};
#elif defined BUILD_FOR_ARM_ARDUINO
Motor motors[] = {
  Motor(ARM_SHOULDER_PIN),
  Motor(ARM_ELBOW_PIN),
  //Motor(ARM_SHOULDER_PIN, ARM_SHOULDER_PIN_D, ARM_SHOULDER_PIN_D2),
  //Motor(ARM_ELBOW_PIN, ARM_ELBOW_PIN_D, ARM_ELBOW_PIN_D2),
  Motor(ARM_BASE_PIN),
  Motor(ARM_WRIST_L, ARM_WRIST_L_DIR1, ARM_WRIST_L_DIR2),
  Motor(ARM_WRIST_R, ARM_WRIST_R_DIR1, ARM_WRIST_R_DIR2),
  Motor(GRIPPER)
  //Motor(ARM_WRIST_YAW_PIN),
  //Motor(ARM_WRIST_ROLL_PIN)
};
#endif
