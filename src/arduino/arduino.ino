#include "main.h"

void setup() {
  // PWM frequency
//  TCCR2B = TCCR2B & B11111000 | B00000001;
//  TCCR1B = TCCR1B & B11111000 | B00000001;
//  TCCR0B = TCCR0B & B11111000 | B00000001;
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);

  Motor::forAll(motors, [](Motor& motor){ motor.init(); });
  Serial.begin(115200);
}

void handle_led(uint32_t state) {
 if (state == OFF) {
  digitalWrite(LED_R, HIGH);
  digitalWrite(LED_G, LOW);
  digitalWrite(LED_B, LOW);
 }
 if (state == MANUAL) {
  digitalWrite(LED_R, LOW);
  digitalWrite(LED_G, HIGH);
  digitalWrite(LED_B, LOW);
 }
 if (state == AUTONOMOUS) {
  digitalWrite(LED_R, HIGH);
  digitalWrite(LED_G, HIGH);
  digitalWrite(LED_B, LOW);
 }
}

void loop() {
  Message* message = Message::read(messageBuffer);
  message->getSpeeds(motors, [](Motor& motor, int32_t speed) {
    motor.setSpeed(speed);
    lastReceived = millis();
  });
  handle_led(message->state);
  
  // 20000 is a magic number and I don't know why but it works
  if (millis() - lastReceived > 20000) {
    Motor::forAll(motors, [](Motor& motor){ motor.setSpeed(0); });
  }
  
  Motor::forAll(motors, [](Motor& motor){ motor.update(); });
  Message::clear(messageBuffer);

  delay(100);
}
