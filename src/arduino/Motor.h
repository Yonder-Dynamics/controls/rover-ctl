//TODO: Don't do this.
#define BUILD_FOR_DRIVETRAIN_ARDUINO
//#define BUILD_FOR_ARM_ARDUINO

struct Motor {
  unsigned char pin;
  unsigned char directionPin;
  unsigned char directionPin2;
  unsigned char maxSpeed = 255;
  bool bidirectional = false;
  bool servoMode = false;
  int32_t goalSpeed = 0;
  int32_t currentSpeed = 0;
  Servo handle;


  static const int32_t maxDelta = 10;
  static const int32_t minPWM = 40;
  static const int32_t zeroPWM = 95;
  static const int32_t maxPWM = 150;

  Motor(unsigned char pin, unsigned char directionPin = 0, unsigned char directionPin2 = 0, bool servoMode = false) {
    this->pin = pin;
    this->directionPin = directionPin;
    this->directionPin2 = directionPin2;
    this->servoMode = servoMode;
    pinMode(pin, OUTPUT);
    if (directionPin > 0) {
      bidirectional = true;
      pinMode(directionPin, OUTPUT);
    } else {
      handle = Servo();
    }
  }

  static int numMotorsFor() {
#if defined BUILD_FOR_DRIVETRAIN_ARDUINO
      return 6;
#elif defined BUILD_FOR_ARM_ARDUINO
      return 6;
#endif
  }

  static void forAll(Motor* motors, void callback(Motor&)) {
    for (int i = 0; i < Motor::numMotorsFor(); i++) {
      Motor& motor = motors[i];
      callback(motor);
    }
  }

  void init() {
    handle.attach(pin);
  }

  void setSpeed(int32_t speed) {
    goalSpeed = clamp(speed, -maxSpeed, maxSpeed);
  }

  void update() {
    int32_t difference = currentSpeed - goalSpeed;
    int32_t delta = clamp(difference, maxDelta, -maxDelta);
    currentSpeed += delta;

    if(this->servoMode){
      currentSpeed = goalSpeed;
    }

    if (bidirectional) {
      analogWrite(pin, abs(currentSpeed));
      digitalWrite(directionPin, currentSpeed > 0 ? HIGH : LOW);
      digitalWrite(directionPin2, currentSpeed > 0 ? LOW : HIGH);
    } else {
      float alpha = abs(goalSpeed / 255.);
      unsigned char speed = goalSpeed > 0 
        ? lerp(zeroPWM, maxPWM, alpha)
        : lerp(zeroPWM, minPWM, alpha);
      handle.write(speed);
    }
  }
};
