struct Message {
  uint32_t numMotors;
  enum System system;
  uint32_t hash;
  uint32_t state;
  int32_t motorSpeeds[6];

  static const char startCharacter = 'a';
  static const int32_t prime = 59359;
  
  static Message* read(char* buffer) {
    char first = Serial.read();
    if (first == startCharacter) {
      while (Serial.available() < sizeof(Message));
      Serial.readBytes(buffer, sizeof(Message));
    }
    return (Message*) buffer;
  }

  void getSpeeds(Motor* motors, void callback(Motor&, int32_t)) {
    if (!isValid()) return;
    for (int i = 0; i < Motor::numMotorsFor(); i++) {
      Motor& motor = motors[i];
      callback(motor, motorSpeeds[i]);
    }
  }

  static void clear(char* buffer) {
    for (int i = 0; i < sizeof(Message); i++) {
      buffer[i] = 0;
    }
  };

  uint32_t computeHash() {
    uint32_t sum = system * prime;
    for (int32_t i = 0; i < numMotors; i++) {
      sum += motorSpeeds[i] * prime;
    }
    return sum;
  }

  bool isValid() {
    return hash == computeHash() &&
      numMotors == Motor::numMotorsFor();
  }
} __attribute__((packed));
