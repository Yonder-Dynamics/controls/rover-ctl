static const int clamp(int v, int lo, int hi) {
  return (v < lo) ? lo : (hi < v) ? hi : v;
}

static int lerp(float a, float b, float f)  {
  return int((a * (1.0 - f)) + (b * f));
}
