enum System: uint32_t {
  Drivetrain = 0,
  Drill = 1,
  Arm = 2
};
