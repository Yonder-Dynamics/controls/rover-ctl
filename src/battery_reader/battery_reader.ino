#define B0_C1S A0
#define B0_C2S A1
#define B0_C3S A2
#define B1_C1S A4
#define B1_C2S A5
#define B1_C3S A6
// board resistor values (kohm)
// I was extremely stupid in layout and as such these are essentially random order
#define R1 100.5 // actually never used, just a pulldown
#define R2 100.2
#define R3 99.8
#define R4 99.7
#define R5 100.1
#define R6 99.7
#define R7 99.6 // actually never used, pulldown
#define R8 99.8
#define R9 99.6
#define R10 99.6
#define R11 99.8
#define R12 100.1

#include <ros.h>
#include <std_msgs/Float64MultiArray.h>


// even with these precise resistor values, nano analog pins aren't super accurate - calibrate against 5V (actually 4.996)
// re-calibration needed for each Arduino!
#define ADC_MAX 1007

#define INTERVAL 1000 // measurement interval in ms

ros::NodeHandle nh;

stg_msgs::Float64MultiArray msg;


msg.data.resize(6); 


ros::Publisher pub("battery_level", &msg);


void setup() {
  nh.initNode();
  nh.advertise(pub);
}

void loop() {
 
  getCellVoltages();
  
  pub.publish(&msg);
  nh.spinOnce();

  delay(INTERVAL);
    
}

void getCellVoltages() {
  // based on resistor layout specific to Aux Arduino Yonder board
  // (can easily make more general in future)
  // saveArray: place to save final voltage readings
  const float maxVoltage = 5.00; // max voltage of ADC
  
  msg.data[0] = maxVoltage*(analogRead(B0_C1S)/ADC_MAX); // first cell is EZ
  // now voltage divider math
  msg.data[1] = (maxVoltage*(analogRead(B0_C2S)/ADC_MAX))*((R2+R3)/R2) - msg.data[0];
  msg.data[2] = (maxVoltage*(analogRead(B0_C3S)/ADC_MAX))*((R4+R5+R6)/R4) - msg.data[0] - msg.data[1];
  

  msg.data[3] = maxVoltage*(analogRead(B1_C1S)/ADC_MAX); // first cell is EZ
  msg.data[4] = (maxVoltage*(analogRead(B1_C2S)/ADC_MAX))*((R7+R8)/R7) - msg.data[3];
  msg.data[5] = (maxVoltage*(analogRead(B1_C3S)/ADC_MAX))*((R10+R11+R12)/R12) - msg.data[3] - msg.data[4];

 
}
