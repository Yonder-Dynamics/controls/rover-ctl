import rospy
import serial, time
import messages
import math

from std_msgs.msg import Float64MultiArray, Int16, String

buffer_count = 0
CLEAR_BUFFER = 2
timeout_time = 1

state_map = {
    "off": 0,
    "manual": 1,
    "autonomous": 2
}

class State:
    def __init__(self):
        self.sub = rospy.Subscriber("state", String, self.update_state)
        self.state = state_map["off"]

    def update_state(self, message):
        self.state = state_map[message.data]

class MotorSystem():
    def __init__(self, target_system, serial_port, callback, activated=True, label="drive"):
        self.speeds = [0,0,0,0,0,0]
        self.serial_port = serial_port
        self.target_system = target_system
        self.callback = callback
        self.time = time.time()
        self.activated = activated
        self.label = label
        self.state = State()

    def listener_callback(self, msg):
        self.time = time.time()
        self.callback(msg, self.speeds)
        #for i, each in enumerate(msg.data):
        #    self.speeds[i] = int(each * 255)
#
#        dt_listener(msg, self.speeds)

#        mess = messages.Message(self.target_system, self.speeds)
#        self.serial_port.write(b'a' + mess.serialize())

    def send_message(self):
        self.speeds = [int(speed) for speed in self.speeds]
        if(self.activated):
            if(time.time() - self.time > 1):
                mess = messages.Message(self.target_system, [0,0,0,0,0,0], self.state.state)
                #  mess = messages.Message(self.target_system, [0,0,0,0,0,0])
            else:
                mess = messages.Message(self.target_system, self.speeds, self.state.state)
                #  mess = messages.Message(self.target_system, self.speeds)

            print(self.target_system, self.speeds, self.state.state)
            self.serial_port.write(b'a' + mess.serialize())



def dt_listener(msg, speeds):
    left = int(msg.data[0])
    right = int(msg.data[1])

    for i, each in enumerate([-right, right, right, -left, -left, -left]):
        speeds[i] = each



drive_ser = serial.Serial("/dev/serial/by-path/platform-3f980000.usb-usb-0:1.2:1.0", 115200)
time.sleep(1)

manual_drive_system = MotorSystem(messages.TARGET_SYSTEMS.DRIVE, drive_ser, dt_listener)
autonomous_drive_system = MotorSystem(messages.TARGET_SYSTEMS.DRIVE, drive_ser, dt_listener, activated=False, label="autonomous")

manual_drive_train_sub = rospy.Subscriber('manual_drive_train', Float64MultiArray, manual_drive_system.listener_callback, queue_size=1)
autonomous_drive_train_sub = rospy.Subscriber('autonomous_drive_train', Float64MultiArray, autonomous_drive_system.listener_callback, queue_size=1)

motor_systems = [manual_drive_system, autonomous_drive_system]

drive_systems = [autonomous_drive_system, manual_drive_system]

## LED CONTROL ###
"""
mode = 0
led_ser = serial.Serial("/dev/serial/by-id/usb-1a86_USB2.0-Ser_-if00-port0", 115200) 

def ledcallback(msg):
  return msg.REACHED_DESTINATION

gate_task = rospy.Subscriber('goto_gps_wp', Float64MultiArray, ledcallback)
"""
###################

def switch_systems(msg):
    global drive_systems
    for i in range(len(drive_systems)):
        if(i == msg.data):
            drive_systems[i].activated = True
        else:
            drive_systems[i].activated = False

rospy.Subscriber('drive_train_multiplexer', Int16, switch_systems, queue_size=1)

rospy.init_node('motor_listener')
rospy.loginfo("started listening for motor commands")
rate = rospy.Rate(10)


while not rospy.is_shutdown():
    if buffer_count > CLEAR_BUFFER:
        for motor_system in motor_systems:
            if(motor_system.activated):
                motor_system.serial_port.reset_input_buffer()
                motor_system.serial_port.reset_output_buffer()
    
    buffer_count += 1

    for motor_system in motor_systems:
        motor_system.send_message()

    if autonomous_drive_system.activated:
        mode = 1
    elif manual_drive_system:
        mode = 2
    elif gate_task: # hopefully this works
        mode = 3
    else:
        mode = 0

    #led_ser.write(mode)

    rate.sleep()

